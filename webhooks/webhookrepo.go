package webhooks

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/high-creek-software/gosnipcart/rest"
	"net/http"
)

const (
	verifyUri = "requestvalidation/%s"
)

var ErrorTokenNotFound = errors.New("snipcart request token not found")
var ErrorValidationFailed = errors.New("validation failed")

type WebhookRepo interface {
	VerifyPayload(r *http.Request) (Event, error)
}

type WebhookRepoImpl struct {
	endpoint *rest.Endpoint
}

func NewWebhookRepo(endpoint *rest.Endpoint) WebhookRepo {
	return &WebhookRepoImpl{endpoint: endpoint}
}

func (w *WebhookRepoImpl) VerifyPayload(r *http.Request) (Event, error) {

	requestToken := r.Header.Get("X-Snipcart-RequestToken")
	if requestToken == "" {
		return Event{}, ErrorTokenNotFound
	}

	req, err := w.endpoint.NewGetRequest(fmt.Sprintf(verifyUri, requestToken))
	if err != nil {
		return Event{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return Event{}, err
	}
	defer resp.Body.Close()

	if w.endpoint.HasError(resp.StatusCode) {
		return Event{}, ErrorValidationFailed
	}

	var validation Validation
	err = json.NewDecoder(resp.Body).Decode(&validation)
	if err != nil {
		return Event{}, err
	}

	if validation.Token != requestToken {
		return Event{}, ErrorValidationFailed
	}

	var event Event
	err = json.NewDecoder(r.Body).Decode(&event)
	defer r.Body.Close()

	return event, err
}
