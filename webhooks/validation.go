package webhooks

import "time"

type Validation struct {
	Token        string    `json:"token"`
	Resource     string    `json:"resource"`
	Expires      time.Time `json:"expires"`
	CreationDate time.Time `json:"creationDate"`
}
