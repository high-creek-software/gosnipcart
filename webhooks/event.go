package webhooks

import (
	"encoding/json"
	"gitlab.com/high-creek-software/gosnipcart/config"
	"gitlab.com/high-creek-software/gosnipcart/notification"
	"gitlab.com/high-creek-software/gosnipcart/orders"
	"time"
)

type Event struct {
	From           string          `json:"from"` // setting to string as it has a different meaning for different event names
	To             string          `json:"to"`   // setting to string as it has a different meaning for different event names
	TrackingNumber string          `json:"trackingNumber"`
	TrackingURL    string          `json:"trackingUrl"`
	EventName      EventName       `json:"eventName"`
	Mode           config.Mode     `json:"mode"`
	CreatedOn      time.Time       `json:"createdOn"`
	Content        json.RawMessage `json:"content"`
}

func (e Event) ParseOrderComplete() (orders.Order, error) {
	var order orders.Order
	err := json.Unmarshal(e.Content, &order)
	return order, err
}

func (e Event) ParseRefund() (orders.Refund, error) {
	var refund orders.Refund
	err := json.Unmarshal(e.Content, &refund)
	return refund, err
}

func (e Event) ParseNotification() (notification.Notification, error) {
	var n notification.Notification
	err := json.Unmarshal(e.Content, &n)
	return n, err
}
