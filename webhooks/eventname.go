package webhooks

import (
	"encoding/json"
	"strings"
)

type EventName int

const (
	EventUnknown EventName = iota
	OrderCompleted
	OrderStatusChanged
	OrderPaymentStatusChanged
	OrderTrackingNumberChanged
	OrderRefundCreated
	OrderNotificationCreated
	SubscriptionSucceeded
	SubscriptionFailed
	SubscriptionCancelRequested
	SubscriptionCancelled
)

var eventNameStrings = []string{"Unknown",
	"order.completed",
	"order.status.changed",
	"order.paymentStatus.changed",
	"order.trackingNumber.changed",
	"order.refund.created",
	"order.notification.created",
	"v3/subscription.invoice.payment.succeeded",
	"v3/subscription.invoice.payment.failed",
	"v3/subscription.state.cancellationRequested",
	"v3/subscription.state.cancelled",
}

func (en EventName) String() string {
	return eventNameStrings[en]
}

func EventNameFromString(en string) EventName {
	for idx, elem := range eventNameStrings {
		if elem == en {
			return EventName(idx)
		}
	}
	return EventUnknown
}

func (en *EventName) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*en = EventNameFromString(str)
	return nil
}

func (en EventName) MarshalJSON() ([]byte, error) {
	return json.Marshal(en.String())
}
