package errors

// Error is an envelope around the request errors
type Error struct {
	Message string `json:"message"`
	Errors  Errors `json:"errors"`
}

// Error implements the error interface for the error envelope
func (e Error) Error() string {
	return e.Message
}

// Errors is a wrapper around the returned errors
type Errors struct {
	Parameter []string `json:"parameter"`
}
