package orders

// Promocode represents what promocode was selected for an order
type Promocode struct {
	Code string `json:"code"`
	Name string `json:"name"`
	Type string `json:"type"`
	Rate int64  `json:"rate"`
}
