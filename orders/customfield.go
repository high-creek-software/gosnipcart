package orders

// CustomField represents a chosen custom field and value.
type CustomField struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
