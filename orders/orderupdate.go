package orders

// OrderUpdate is the data type required to update an order
type OrderUpdate struct {
	Token          string         `json:"token"`
	Status         Status         `json:"status"`
	PaymentStatus  string         `json:"paymentStatus,omitempty"`
	TrackingNumber string         `json:"trackingNumber,omitempty"`
	TrackingUrl    string         `json:"trackingUrl,omitempty"`
	Metadata       map[string]any `json:"metadata,omitempty"`
}
