package orders

type Wrapper struct {
	TotalItems int64   `json:"totalItems"`
	Offset     int64   `json:"offset"`
	Limit      int64   `json:"limit"`
	Items      []Order `json:"items"`
}
