package orders

import "time"

type OrderQuery struct {
	Status           string
	InvoiceNumber    string
	ProductID        string
	PlacedBy         string
	From             *time.Time
	To               *time.Time
	IsRecurringOrder bool
}
