package orders

import (
	"bytes"
	"encoding/json"
	"gitlab.com/high-creek-software/gosnipcart/rest"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	uri = "orders"
)

type OrderRepo interface {
	GetOrders(offset, limit int64, query OrderQuery) (Wrapper, error)
	GetOrder(token string) (Order, error)
	UpdateOrder(token string, update OrderUpdate) (Order, error)
}

type OrderRepoImpl struct {
	endpoint *rest.Endpoint
}

// NewOrderRepo creates an OrderRepo
func NewOrderRepo(e *rest.Endpoint) OrderRepo {
	return &OrderRepoImpl{endpoint: e}
}

// GetOrders uses the offset and limit along with the OrderQuery to help fetch orders
func (r *OrderRepoImpl) GetOrders(offset, limit int64, query OrderQuery) (Wrapper, error) {

	req, err := r.endpoint.NewGetRequest(uri)
	if err != nil {
		return Wrapper{}, err
	}

	q := url.Values{}
	q.Add("offset", strconv.FormatInt(offset, 10))
	q.Add("limit", strconv.FormatInt(limit, 10))

	if query.Status != "" {
		q.Add("status", query.Status)
	}
	if query.InvoiceNumber != "" {
		q.Add("invoiceNumber", query.InvoiceNumber)
	}
	if query.ProductID != "" {
		q.Add("productId", query.ProductID)
	}
	if query.PlacedBy != "" {
		q.Add("placedBy", query.PlacedBy)
	}
	if query.From != nil {
		q.Add("from", query.From.Format(time.RFC3339))
	}
	if query.To != nil {
		q.Add("to", query.To.Format(time.RFC3339))
	}
	if query.IsRecurringOrder {
		q.Add("isRecurringOrder", "true")
	}

	req.URL.RawQuery = q.Encode()

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		return Wrapper{}, nil
	}
	defer resp.Body.Close()

	if r.endpoint.HasError(resp.StatusCode) {
		return Wrapper{}, r.endpoint.ParseError(resp.Body)
	}

	var wrapper Wrapper
	err = json.NewDecoder(resp.Body).Decode(&wrapper)

	return wrapper, err
}

// GetOrder looks for the order by token
func (r *OrderRepoImpl) GetOrder(token string) (Order, error) {

	orderURI, err := url.JoinPath(uri, token)
	if err != nil {
		return Order{}, err
	}

	req, err := r.endpoint.NewGetRequest(orderURI)
	if err != nil {
		return Order{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return Order{}, err
	}
	defer resp.Body.Close()

	if r.endpoint.HasError(resp.StatusCode) {
		return Order{}, r.endpoint.ParseError(resp.Body)
	}

	var order Order
	err = json.NewDecoder(resp.Body).Decode(&order)

	return order, err
}

// UpdateOrder updates the given order (by token)
func (r *OrderRepoImpl) UpdateOrder(token string, update OrderUpdate) (Order, error) {

	orderURI, err := url.JoinPath(uri, token)
	if err != nil {
		return Order{}, err
	}

	encoded, err := json.Marshal(update)
	if err != nil {
		return Order{}, err
	}
	reader := bytes.NewBuffer(encoded)

	req, err := r.endpoint.NewPutRequest(orderURI, reader)
	if err != nil {
		return Order{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return Order{}, err
	}

	if r.endpoint.HasError(resp.StatusCode) {
		return Order{}, r.endpoint.ParseError(resp.Body)
	}

	var order Order
	err = json.NewDecoder(resp.Body).Decode(&order)

	return order, err
}
