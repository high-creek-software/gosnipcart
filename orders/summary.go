package orders

import "encoding/json"

type Summary struct {
	Subtotal                      float64         `json:"subtotal"`
	TaxableTotal                  float64         `json:"taxableTotal"`
	Total                         float64         `json:"total"`
	PayableNow                    float64         `json:"payableNow"`
	PaymentMethod                 PaymentMethod   `json:"paymentMethod"`
	Taxes                         []Tax           `json:"taxes"`
	DiscountInducedTaxesVariation float64         `json:"discountInducedTaxesVariation"`
	AdjustedTotal                 float64         `json:"adjustedTotal"`
	Shipping                      json.RawMessage `json:"shipping"`
}
