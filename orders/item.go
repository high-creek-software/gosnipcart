package orders

// Item represents an item added to an order
type Item struct {
	UniqueId         string        `json:"uniqueId"`
	Id               string        `json:"id"`
	Name             string        `json:"name"`
	Price            float64       `json:"price"`
	Quantity         int64         `json:"quantity"`
	Url              string        `json:"url"`
	Weight           float64       `json:"weight"`
	Description      string        `json:"description"`
	Image            string        `json:"image"`
	CustomFieldsJson []CustomField `json:"customFieldsJson"`
	Stackable        bool          `json:"stackable"`
	MaxQuantity      int64         `json:"maxQuantity"`
	TotalPrice       float64       `json:"totalPrice"`
	TotalWeight      float64       `json:"totalWeight"`
}
