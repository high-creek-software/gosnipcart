package orders

import (
	"encoding/json"
	"strings"
)

// Status represents an order status
type Status int

const (
	Unknown Status = iota
	InProgress
	Processed
	Disputed
	Shipped
	Delivered
	Pending
	Cancelled
)

var statusStrings = []string{"Unknown", "InProgress", "Processed", "Disputed", "Shipped", "Delivered", "Pending", "Cancelled"}

// String turn a status into a string
func (s Status) String() string {
	return statusStrings[s]
}

// StatusFromString get a status from an incoming string
func StatusFromString(s string) Status {
	for idx, elem := range statusStrings {
		if elem == s {
			return Status(idx)
		}
	}
	return Unknown
}

// UnmarshalJSON unmarshal form json
func (s *Status) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*s = StatusFromString(str)
	return nil
}

// MarshalJSON marshal to json
func (s Status) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.String())
}
