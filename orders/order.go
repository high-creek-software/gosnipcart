package orders

import (
	"gitlab.com/high-creek-software/gosnipcart/address"
	"gitlab.com/high-creek-software/gosnipcart/user"
	"time"
)

// Order is a snipcart order
type Order struct {
	Token                 string           `json:"token"`
	CreationDate          time.Time        `json:"creationDate"`
	ModificationDate      time.Time        `json:"modificationDate"`
	Status                Status           `json:"status"`
	PaymentMethod         string           `json:"paymentMethod"`
	InvoiceNumber         string           `json:"invoiceNumber"`
	Email                 string           `json:"email"`
	CardHolderName        string           `json:"cardHolderName"`
	CreditCardLast4Digits string           `json:"creditCardLast4Digits"`
	BillingAddress        *address.Address `json:"billingAddress"`
	Notes                 string           `json:"notes"`
	ShippingAddress       *address.Address `json:"shippingAddress"`
	User                  *user.User       `json:"user"`
	FinalGrandTotal       float64          `json:"finalGrandTotal"`
	ShippingFees          int64            `json:"shippingFees"`
	ShippingMethod        string           `json:"shippingMethod"`
	Items                 []Item           `json:"items"`
	Discounts             []Discount       `json:"discounts"`
	Taxes                 []Tax            `json:"taxes"`
	RebateAmount          float64          `json:"rebateAmount"`
	Subtotal              float64          `json:"subtotal"`
	ItemsTotal            float64          `json:"itemsTotal"`
	GrandTotal            float64          `json:"grandTotal"`
	TotalWeight           float64          `json:"totalWeight"`
	HasPromoCode          bool             `json:"hasPromoCode"`
	TotalRebateRate       float64          `json:"totalRebateRate"`
	Promocodes            []Promocode      `json:"promocodes"`
	WillBePaidLater       bool             `json:"willBePaidLater"`
	CustomFields          []CustomField    `json:"customFields"`
	PaymentTransactionId  string           `json:"PaymentTransactionId"`
	TrackingNumber        string           `json:"trackingNumber"`
	TrackingURL           string           `json:"trackingUrl"`
	Summary               Summary          `json:"summary"`
	NumberOfItemsInOrder  int64            `json:"numberOfItemsInOrder"`
	UserAgent             string           `json:"userAgent"`
	address.NestedAddress
}
