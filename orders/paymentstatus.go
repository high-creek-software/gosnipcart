package orders

import (
	"encoding/json"
	"strings"
)

type PaymentStatus int

const (
	PaymentStatusUnknown PaymentStatus = iota
	Authorized
	Paid
)

var paymentStatusStrings = []string{"Unknown", "Authorized", "Paid"}

func (p PaymentStatus) String() string {
	return paymentStatusStrings[p]
}

func PaymentStatusFromString(p string) PaymentStatus {
	ps := PaymentStatusUnknown
	for idx, elem := range paymentStatusStrings {
		if elem == p {
			ps = PaymentStatus(idx)
		}
	}
	return ps
}

func (p *PaymentStatus) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), `"`)
	*p = PaymentStatusFromString(s)
	return nil
}

func (p PaymentStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}
