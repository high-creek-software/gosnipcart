package orders

import "time"

type Discount struct {
	AmountSaved      float64   `json:"amountSaved"`
	DiscountID       string    `json:"discountId"`
	Name             string    `json:"name"`
	Combinable       bool      `json:"combinable"`
	Trigger          string    `json:"trigger"` // TODO: Trigger turn into eum-ish
	Code             string    `json:"code"`
	Type             string    `json:"type"` // TODO: Type turn into enum-ish
	Amount           float64   `json:"amount"`
	Id               string    `json:"id"`
	CreationDate     time.Time `json:"creationDate"`
	ModificationDate time.Time `json:"modificationDate"`
}
