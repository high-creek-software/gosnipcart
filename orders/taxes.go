package orders

// Tax represents tax charged for an order
type Tax struct {
	TaxName          string  `json:"taxName"`
	TaxRate          float64 `json:"taxRate"`
	Amount           float64 `json:"amount"`
	NumberForInvoice string  `json:"numberForInvoice"`
}
