package orders

// Refund represents a refund notification from snipcart
type Refund struct {
	OrderToken              string  `json:"orderToken"`
	Amount                  float64 `json:"amount"`
	Comment                 string  `json:"comment"`
	NotifiedCustomerByEmail bool    `json:"notifiedCustomerByEmail"`
	Currency                string  `json:"currency"`
}
