package orders

import (
	"encoding/json"
	"strings"
)

type PaymentMethod int

const (
	PaymentMethodUnknown PaymentMethod = iota
	CreditCard
)

var paymentMethodStrings = []string{"Unknown", "CreditCard"}

func (pm PaymentMethod) String() string {
	return paymentMethodStrings[pm]
}

func PaymentMethodFromString(pm string) PaymentMethod {
	for idx, elem := range paymentMethodStrings {
		if elem == pm {
			return PaymentMethod(idx)
		}
	}
	return PaymentMethodUnknown
}

func (pm *PaymentMethod) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*pm = PaymentMethodFromString(str)
	return nil
}

func (pm *PaymentMethod) MarshalJSON() ([]byte, error) {
	return json.Marshal(pm.String())
}
