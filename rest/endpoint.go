package rest

import (
	"encoding/json"
	"fmt"
	"gitlab.com/high-creek-software/gosnipcart/errors"
	"io"
	"net/http"
)

const (
	headerAccept        = "Accept"
	headerContentType   = "Content-Type"
	applicationJson     = "application/json"
	headerAuthorization = "Authorization"
	basicFormat         = "Basic %s"
)

// Endpoint holds the rest for the snipcart api
type Endpoint struct {
	apiKey  string
	baseURL string
}

// NewEndpoint creates a new endpoint
func NewEndpoint(apiKey, baseURL string) *Endpoint {
	return &Endpoint{apiKey: apiKey, baseURL: baseURL}
}

// UpdateBaseURL allows the user to override the set url
func (e *Endpoint) UpdateBaseURL(baseURL string) {
	e.baseURL = baseURL
}

// NewGetRequest creates a new get request with the proper json header and authentication
func (e *Endpoint) NewGetRequest(uri string) (*http.Request, error) {
	url := e.baseURL + uri

	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add(headerAccept, applicationJson)
	req.Header.Add(headerAuthorization, fmt.Sprintf(basicFormat, e.apiKey))

	return req, nil
}

func (e *Endpoint) NewPutRequest(uri string, body io.Reader) (*http.Request, error) {
	url := e.baseURL + uri

	req, err := http.NewRequest(http.MethodPut, url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add(headerAccept, applicationJson)
	req.Header.Add(headerContentType, applicationJson)
	req.Header.Add(headerAuthorization, fmt.Sprintf(basicFormat, e.apiKey))

	return req, nil
}

// HasError checks the status code to see if it has an error value
func (e *Endpoint) HasError(statusCode int) bool {
	if statusCode == 200 || statusCode == 201 || statusCode == 204 {
		return false
	}
	return true
}

// ParseError takes a reader from the response and parses it into the error struct
func (e *Endpoint) ParseError(r io.Reader) error {
	var parseErr errors.Error
	err := json.NewDecoder(r).Decode(&parseErr)
	if err != nil {
		return err
	}
	return parseErr
}
