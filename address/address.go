package address

import "encoding/json"

type Address struct {
	FullName               string          `json:"fullName"`
	FirstName              string          `json:"firstName"`
	Name                   string          `json:"name"`
	Company                string          `json:"company"`
	Address1               string          `json:"address1"`
	Address2               string          `json:"address2"`
	FullAddress            string          `json:"fullAddress"`
	City                   string          `json:"city"`
	Country                string          `json:"country"`
	PostalCode             string          `json:"postalCode"`
	Province               string          `json:"province"`
	Phone                  string          `json:"phone"`
	VatNumber              int             `json:"vatNumber"`
	HasMinimalRequiredInfo bool            `json:"hasMinimalRequiredInfo"`
	ValidationErrors       json.RawMessage `json:"validationErrors"`
}
