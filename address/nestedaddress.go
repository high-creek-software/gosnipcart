package address

type NestedAddress struct {
	BillingAddressName           string `json:"billingAddressName"`
	BillingAddressCompanyName    string `json:"billingAddressCompanyName"`
	BillingAddressAddress1       string `json:"billingAddressAddress1"`
	BillingAddressAddress2       string `json:"billingAddressAddress2"`
	BillingAddressCity           string `json:"billingAddressCity"`
	BillingAddressCountry        string `json:"billingAddressCountry"`
	BillingAddressProvince       string `json:"billingAddressProvince"`
	BillingAddressPostalCode     string `json:"billingAddressPostalCode"`
	BillingAddressPhone          string `json:"billingAddressPhone"`
	ShippingAddressName          string `json:"shippingAddressName"`
	ShippingAddressCompanyName   string `json:"shippingAddressCompanyName"`
	ShippingAddressAddress1      string `json:"shippingAddressAddress1"`
	ShippingAddressAddress2      string `json:"shippingAddressAddress2"`
	ShippingAddressCity          string `json:"shippingAddressCity"`
	ShippingAddressCountry       string `json:"shippingAddressCountry"`
	ShippingAddressProvince      string `json:"shippingAddressProvince"`
	ShippingAddressPostalCode    string `json:"shippingAddressPostalCode"`
	ShippingAddressPhone         string `json:"shippingAddressPhone"`
	ShippingAddressSameAsBilling bool   `json:"shippingAddressSameAsBilling"`
}
