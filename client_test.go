package gosnipcart_test

import (
	"gitlab.com/high-creek-software/gosnipcart"
	"gitlab.com/high-creek-software/gosnipcart/orders"
	"log"
	"os"
	"testing"
)

func TestGetOrders(t *testing.T) {
	key := os.Getenv("SNIPCART_KEY")

	if key == "" {
		t.Fatalf("Expected key")
	}

	client := gosnipcart.NewClient(key)

	wrapper, err := client.GetOrders(0, 50, orders.OrderQuery{})
	if err != nil {
		t.Fatal("error getting orders", err)
	}

	log.Println(wrapper)
}
