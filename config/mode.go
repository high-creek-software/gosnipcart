package config

import (
	"encoding/json"
	"strings"
)

type Mode int

const (
	ModeUnknown Mode = iota
	ModeTest
	ModeLive
)

var modeStrings = []string{"Unknown", "Test", "Live"}

func (m Mode) String() string {
	return modeStrings[m]
}

func ModeFromString(m string) Mode {
	for idx, elem := range modeStrings {
		if elem == m {
			return Mode(idx)
		}
	}
	return ModeUnknown
}

func (m *Mode) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*m = ModeFromString(str)
	return nil
}

func (m Mode) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.String())
}
