package user

import (
	"gitlab.com/high-creek-software/gosnipcart/address"
	"gitlab.com/high-creek-software/gosnipcart/config"
	"time"
)

type User struct {
	ID              string           `json:"id"`
	Email           string           `json:"email"`
	Mode            config.Mode      `json:"mode"`
	Statistics      Statistics       `json:"statistics"`
	CreationDate    time.Time        `json:"creationDate"`
	BillingAddress  *address.Address `json:"billingAddress"`
	ShippingAddress *address.Address `json:"shippingAddress"`
	Status          string           `json:"status"` // TODO: Turn into enum-ish
	SessionToken    string           `json:"sessionToken"`
	GravatarURL     string           `json:"gravatarUrl"`
}
