package user

type Statistics struct {
	OrdersCount        int64   `json:"ordersCount"`
	OrdersAmount       float64 `json:"ordersAmount"`
	SubscriptionsCount int64   `json:"subscriptionsCount"`
}
