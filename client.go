package gosnipcart

import (
	"encoding/base64"
	"gitlab.com/high-creek-software/gosnipcart/orders"
	"gitlab.com/high-creek-software/gosnipcart/rest"
	"gitlab.com/high-creek-software/gosnipcart/webhooks"
)

const (
	defaultBaseURL = "https://app.snipcart.com/api/"
)

// Client holds the api key to access the snipcart resources.
type Client struct {
	endpoint *rest.Endpoint
	orders.OrderRepo
	webhooks.WebhookRepo
}

type ClientConfig func(c *Client)

// NewClient takes a plain text api key, base64 encodes it with a trailing colon `:` and returns the client.
func NewClient(apiKey string, opts ...ClientConfig) *Client {
	encoded := base64.StdEncoding.EncodeToString([]byte(apiKey + ":"))

	client := &Client{endpoint: rest.NewEndpoint(encoded, defaultBaseURL)}
	client.OrderRepo = orders.NewOrderRepo(client.endpoint)
	client.WebhookRepo = webhooks.NewWebhookRepo(client.endpoint)

	for _, opt := range opts {
		opt(client)
	}

	return client
}

// SetBaseURL updates the base url used by the client.
func SetBaseURL(baseURL string) ClientConfig {
	return func(c *Client) {
		c.endpoint.UpdateBaseURL(baseURL)
	}
}
