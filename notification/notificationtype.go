package notification

import (
	"encoding/json"
	"strings"
)

type NotificationType int

const (
	Unkown NotificationType = iota
	Invoice
	Comment
	TrackingNumber
	OrderCancelled
	Refund
	OrderShipped
	OrderReceived
	OrderPaymentExpired
	OrderStatusChanged
	RecoveryCampaign
	DigitalDownload
	Logs
	Other
)

var notificationStrings = []string{"Invoice", "Comment", "TrackingNumber", "OrderCancelled", "Refund", "OrderShipped", "OrderReceived", "OrderPaymentExpired", "OrderStatusChanged", "RecoveryCampaign", "DigitalDownload", "Logs", "Other"}

func (n NotificationType) String() string {
	return notificationStrings[n]
}

func NotificationTypeFromString(n string) NotificationType {
	for idx, elem := range notificationStrings {
		if elem == n {
			return NotificationType(idx)
		}
	}
	return Unkown
}

func (n *NotificationType) UnmarshalJSON(b []byte) error {
	str := strings.Trim(string(b), `"`)
	*n = NotificationTypeFromString(str)
	return nil
}

func (n NotificationType) MarshalJSON() ([]byte, error) {
	return json.Marshal(n.String())
}
