package notification

import "time"

type Notification struct {
	NotificationType NotificationType `json:"notificationType"`
	SentByEmailOn    *time.Time       `json:"sentByEmailOn"`
	SentByEmail      bool             `json:"sentByEmail"`
	OrderToken       string           `json:"orderToken"`
	Message          string           `json:"message"`
	ResourceURL      string           `json:"resourceUrl"`
	Subject          string           `json:"subject"`
}
